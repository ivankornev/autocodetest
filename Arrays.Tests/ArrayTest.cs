using NUnit.Framework;
using System;
using Arrays;

namespace Arrays.Tests
{
    public class ArrayTest
    {
        [Test]
        [TestCase(null, SortOrder.Ascending)]
        [TestCase(null, SortOrder.Descending)]
        public void IsSorted_NullArgumentArrayAscendingSort_ThrowArgumentException(int[] array, SortOrder order)
        {
            Assert.Throws<ArgumentNullException>(() =>
                    Operations.IsSorted(array, order));
        }

        [Test]
        [TestCase(new int[] { 10, 20, 30 }, SortOrder.Ascending, ExpectedResult = true)]
        [TestCase(new int[] { 30, 20, 10 }, SortOrder.Descending, ExpectedResult = true)]
        [TestCase(new int[] { 30, 30, 30 }, SortOrder.Ascending, ExpectedResult = true)]
        [TestCase(new int[] { 30, 30, 30 }, SortOrder.Descending, ExpectedResult = true)]
        [TestCase(new int[] { 10, 20, 30 }, SortOrder.Descending, ExpectedResult = false)]
        [TestCase(new int[] { 30, 20, 10 }, SortOrder.Ascending, ExpectedResult = false)]
        [TestCase(new int[] { 30, -7, 30 }, SortOrder.Ascending, ExpectedResult = false)]
        [TestCase(new int[] { 90, 30, 100 }, SortOrder.Descending, ExpectedResult = false)]
        public bool IsSorted_NotNullArraySorted_ResultReturned(int[] array, SortOrder order) =>
            Operations.IsSorted(array, order);
    }
}